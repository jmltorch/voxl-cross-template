/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>  // for fprintf
#include <unistd.h> 
#include <getopt.h>
#include <string.h>
#include <modal_pipe.h>
#include <voxl_cutils.h>

#include "hello_cross.h"

#define CLIENT_NAME         "hello-cross"

static char en_newline = 0;
static char imu_name[64];

// -----------------------------------------------------------------------------------------------------------------------------
// Print the help message
// -----------------------------------------------------------------------------------------------------------------------------
static void _print_usage()
{
    printf("\nCommand line arguments are as follows:\n\n");
    printf("-h, --help         : Print this help message\n");
    printf("-i, --input_imu    : imu pipe to show (required)\n");
    printf("-n, --new_line     : prints data on new lines instead of overwriting\n");
    printf("\nFor example: voxl-hello-cross -i imu0\n");
}

// -----------------------------------------------------------------------------------------------------------------------------
// Parses the command line arguments to the main function
// -----------------------------------------------------------------------------------------------------------------------------
static int _parse_opts(int         argc,            ///< Number of arguments
		              char* const argv[])          ///< arguments
{
    static struct option LongOptions[] =
    {
        {"help",          no_argument,        0, 'h'},
        {"input_imu",     required_argument,  0, 'i'},
        {"new_line",      no_argument,        0, 'n'},
    };

    int optionIndex      = 0;
    int status           = 0;
    int option;

    while ((status == 0) && (option = getopt_long (argc, argv, "hi:n", &LongOptions[0], &optionIndex)) != -1)
    {
        switch(option)
        {

            case 'h':
                status = -1;
                break;

            case 'i':
                sscanf(optarg, "%s", imu_name);

                break;

            case 'n':
            	en_newline = 1;
            	break;

            // Unknown argument
            case '?':
            default:
                printf("Invalid argument passed!\n");
                status = -1;
                break;
        }
    }

    if(status != 0) return status;

    if(strlen(imu_name) == 0){
        printf("Missing required argument: input imu\n\n");
        return -1;
    }

    return status;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
    fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
    return;
}

// called when the simple helper has data for us
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
    // validate that the data makes sense
    int n_packets;
    imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);
    if(data_array == NULL) return;

    // print everything in one go.
    if(!en_newline) printf("\r" CLEAR_LINE);
    printf("%7.2f|%7.2f|%7.2f",
    (double)data_array[n_packets-1].accl_ms2[0],
    (double)data_array[n_packets-1].accl_ms2[1],
    (double)data_array[n_packets-1].accl_ms2[2]);

    if(en_newline) printf("\n");
    
    fflush(stdout);
    return;
}
int main(int argc, char* const argv[]){

    if(_parse_opts(argc, argv)){
        _print_usage();
        return -1;
    }

    enable_signal_handler();
    main_running = 1;
    //disable terminal wrapping
    printf(DISABLE_WRAP);

    printHelloCross();

    // set up all our MPA callbacks
    pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
    //pipe_client_set_connect_cb(0, _connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

    printf("waiting for server at: %s\r", imu_name);
    fflush(stdout);
    int ret = pipe_client_open(0, imu_name, CLIENT_NAME, \
                EN_PIPE_CLIENT_SIMPLE_HELPER, \
                IMU_RECOMMENDED_READ_BUF_SIZE);

    // check for errors trying to connect to the server pipe
    if(ret<0){
        pipe_print_error(ret);
        printf(ENABLE_WRAP);
        return -1;
    }

    // keep going until the  signal handler sets the running flag to 0
    while(main_running) usleep(500000);

    // all done, signal pipe read threads to stop
    printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
    pipe_client_close_all();
}

void printHelloCross(){

    printf("Voxl Hello Cross!!!!!\n\n");
    printf("   Imu Acceleration\n");
    printf("   X   |   Y   |   Z\n");

}
